import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchByAssetsComponent } from './search-by-assets.component';

describe('SearchByAssetsComponent', () => {
  let component: SearchByAssetsComponent;
  let fixture: ComponentFixture<SearchByAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchByAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchByAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
