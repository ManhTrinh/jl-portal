import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-search-by-asset',
  templateUrl: './search-by-asset.component.html',
  styleUrls: ['./search-by-asset.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SearchByAssetComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
