import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-engineer-tracking',
  templateUrl: './engineer-tracking.component.html',
  styleUrls: ['./engineer-tracking.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EngineerTrackingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
