import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineerTrackingComponent } from './engineer-tracking.component';

describe('EngineerTrackingComponent', () => {
  let component: EngineerTrackingComponent;
  let fixture: ComponentFixture<EngineerTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineerTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineerTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
