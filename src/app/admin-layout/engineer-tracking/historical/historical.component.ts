import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-historical',
  templateUrl: './historical.component.html',
  styleUrls: ['./historical.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HistoricalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
