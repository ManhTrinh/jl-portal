import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoicesComponent implements OnInit {
  jobs: Observable<any[]>;
  length = 100;
  pageSize = 10;
  pageSizeOptions = [5, 10, 25, 100];

  constructor(db: AngularFirestore) {
    this.jobs = db.collection('jobs').valueChanges();
  }

  ngOnInit() {
  }

}
