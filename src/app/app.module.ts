import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing';

// API Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';

// Helpers
import { MaterialDesignModule } from './material.module';
import { AgmCoreModule } from '@agm/core';

// App Components
import {
  AppComponent,
  AdminLayoutComponent,
  LogInComponent,
  JobsComponent,
  SearchComponent,
  SearchByAssetComponent,
  VisitsComponent,
  QuotesComponent,
  InvoicesComponent,
  SitesComponent,
  CustomerComponent,
  DashboardsComponent,
  EngineerTrackingComponent,
  LiveComponent,
  HistoricalComponent,
  PageNotFoundComponent
} from './';

@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    PageNotFoundComponent,
    AdminLayoutComponent,
    SearchComponent,
    SearchByAssetComponent,
    JobsComponent,
    VisitsComponent,
    QuotesComponent,
    InvoicesComponent,
    SitesComponent,
    CustomerComponent,
    DashboardsComponent,
    EngineerTrackingComponent,
    LiveComponent,
    HistoricalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    // API Firebase
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,

    // Helpers
    MaterialDesignModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD75jJzYPTISNdEB_vBxJMjey9phrPHTsw'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
