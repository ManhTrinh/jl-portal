import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import {
  LogInComponent,
  AdminLayoutComponent,
  JobsComponent,
  SearchComponent,
  SearchByAssetComponent,
  VisitsComponent,
  QuotesComponent,
  InvoicesComponent,
  SitesComponent,
  CustomerComponent,
  DashboardsComponent,
  EngineerTrackingComponent,
  LiveComponent,
  HistoricalComponent,


  // page not found 404 error
  PageNotFoundComponent
} from './index';

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        component: JobsComponent,
        children: [
          {
            path: '',
            component: SearchComponent
          },
          {
            path: 'jobs',
            component: SearchComponent
          },
          {
            path: 'jobs/search-by-asset',
            component: SearchByAssetComponent
          }
        ]
      },
      {
        path: 'visits',
        component: VisitsComponent
      },
      {
        path: 'quotes',
        component: QuotesComponent
      },
      {
        path: 'invoices',
        component: InvoicesComponent
      },
      {
        path: 'sites',
        component: SitesComponent
      },
      {
        path: 'customer',
        component: CustomerComponent
      },
      {
        path: 'dashboards',
        component: DashboardsComponent
      },
      {
        path: 'engineer-tracking',
        component: EngineerTrackingComponent,
        children: [
          {
            path: '',
            component: HistoricalComponent
          },
          {
            path: 'live',
            component: LiveComponent
          }
        ]
      },
    ]
  },
  { path: 'account/login', component: LogInComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
