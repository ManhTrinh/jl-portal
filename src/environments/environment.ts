// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDP-7IvCF5iFKIdAoY_Es9PEl1tJYQRb-k',
    authDomain: 'jl-portal.firebaseapp.com',
    databaseURL: 'https://jl-portal.firebaseio.com',
    projectId: 'jl-portal',
    storageBucket: '',
    messagingSenderId: '79364186530'
  }
};
